//mutex class
class Mutex
{
	public:
		//the default constructor
		Mutex()
		{
			InitializeCriticalSection(&m_criticalSection);
		}

		//destructor
		~Mutex()
		{
			DeleteCriticalSection(&m_criticalSection);
		}

		//lock
		void lock()
		{
			EnterCriticalSection(&m_criticalSection);
		}

		//unlock
		void unlock()
		{
			LeaveCriticalSection(&m_criticalSection);
		}

	private:
		CRITICAL_SECTION m_criticalSection;
};