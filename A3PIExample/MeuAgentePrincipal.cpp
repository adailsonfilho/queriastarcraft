#include "MeuAgentePrincipal.h"
#include <AgentePrincipal.h>
#include <Windows.h>
#include <BWAPI\Position.h>
#include <BWAPI\UnitType.h>
#include "DecisionsEnum.cpp"
#include "Actor.cpp"
#include <vector>
#include <iostream>
#include <math.h>

using namespace std;

//blackboard!
bool GameOver = false;
Unidade* amigoDaVez = NULL;
Unidade* scout = NULL;
Unidade* nexus = NULL;

int ** quadrant;
int tileSize = 0;
int mapH = 0;//size in tiles
int mapW = 0;//size in tiles
int newProbeRequest = 3;
int newPylonRequest = 1;
int newGatewayRequest = 2;
int newZealotRequest = 40;

vector<Actor> probes;
vector<Unidade*> pylons;
vector<Unidade*> buidings;

set<Unidade*> enemies;

//controle de custos
int mineralsReserved = 0;
int gasReserved = 0;

HANDLE scoutMutex, reserveMineralsMutex, mineralsSetMutex;
Unidade *scoutReference = NULL;

struct compararPosicao {
	bool operator() (Unidade* a, Unidade* b) const {
		return (a->getTilePosition().x() != b->getTilePosition().x() && a->getTilePosition().y() != b->getTilePosition().y());
	}
};
set<Unidade*, compararPosicao> mineralsSet;
vector<EDecision> pendingDecisions;

void makeScout(Unidade* u);

void AITrabalhador (Unidade* u){


	//Varre todos os minerais ja encontrados
	int distance = 9999999;

	Unidade* mineralPerto = NULL;
	Unidade* reference;

	if(nexus == NULL){
		reference = u;
	}else{
		reference = nexus;
	}

	printf("procurar mineral proximo em lista com %d items\n",mineralsSet.size());

	DWORD dwWaitResult, pegueiAcesso=0; 
	dwWaitResult = WaitForSingleObject( 
	mineralsSetMutex,   // handle to mutex
	INFINITE);			// no time-out interval

	while( pegueiAcesso < 1 ){ 
		switch (dwWaitResult) {
			// The thread got ownership of the mutex
			case WAIT_OBJECT_0: 
				//Se este mineral acabou
				for(set<Unidade*, compararPosicao>::iterator it = mineralsSet.begin(); it != mineralsSet.end(); ++it){
					if((*it)->getResources() == 0){
						mineralsSet.erase(it);
						printf("Mineral removido, no tile %d %d\n",(*it)->getTilePosition().x(),(*it)->getTilePosition().y());
					}else if(reference->getDistance(*it) < distance){
						//se n�o, calcula a distancia e pega o mais perto do nexus (se houver um)
						distance = reference->getDistance(*it);
						mineralPerto = (*it);
					}//vai minerar no mineral mais perto
					if(mineralPerto != NULL){
						u->rightClick(mineralPerto);
						printf("Minerando em tile: %d %d\n",mineralPerto->getTilePosition().x(),mineralPerto->getTilePosition().y());
					}
				}
				pegueiAcesso++;
				if (! ReleaseMutex(mineralsSetMutex)) { 
						// Handle error. Heh.
				}
		}
	}
}

void getExplorationPoint(int quadrant_x, int quadrant_y){

}

BWAPI::Position getQuadrantCentroid(int i, int j){

	//printf("Scout go to quadrant: %d %d\n", i,j);

	//pega em quantos quadrantes o mapa esta divido
	int quadrant_lines = sizeof(quadrant);
	int quadrant_columns = sizeof(quadrant[0]);

	//tamanho dos quadrantes em px (se tirar a multiplica��o, fica em tiles)
	int quadrant_width = (mapW/quadrant_columns)*tileSize;
	int quadrant_height = (mapH/quadrant_lines)*tileSize;

	//calcula centroid do quadrante
	int x = (i*quadrant_width) + (quadrant_width/2);
	int y = (j*quadrant_height) + (quadrant_height/2);

	//printf("Cetroid do quadrante %d %d: %d %d\n",i,j,x,y);

	return BWAPI::Position(x,y);
}

BWAPI::Position getQuadrant(Unidade* unidade){
	BWAPI::Position p = unidade->getPosition();

	//pega em quantos quadrantes o mapa esta divido
	int quadrant_lines = sizeof(quadrant);
	int quadrant_columns = sizeof(quadrant[0]);

	//tamanho dos quadrantes em px (se tirar a multiplica��o, fica em tiles)
	int quadrant_width = (mapW/quadrant_columns)*tileSize;
	int quadrant_height = (mapH/quadrant_lines)*tileSize;

	//locate current scout quadrant
	double coef_horizontal = p.x()/((double)quadrant_width);
	double coef_vertical = p.y()/((double)quadrant_height);

	//indice do quadrant atual
	int i = floor(coef_horizontal);
	int j = floor(coef_vertical);

	printf("Posi��o do scout: %d %d\n",p.x(),p.y());
	printf("quadrante: %d %d\n",i,j);

	return BWAPI::Position(i,j);
}

void updateEnviromentInfo(Unidade* unidade){

	DWORD dwWaitResult, pegueiAcesso=0; 
	dwWaitResult = WaitForSingleObject( 
	mineralsSetMutex,   // handle to mutex
	INFINITE);			// no time-out interval

	while( pegueiAcesso < 1 ){ 
		switch (dwWaitResult) {
			// The thread got ownership of the mutex
			case WAIT_OBJECT_0: 
				//Se este mineral acabou
				
				//registra minerais encontrados
				if(unidade->exists()){
					cout << "minerals set" << mineralsSet.size() << endl;
					set<Unidade*> mineralsSeen = unidade->getMinerals() ;
					cout << "minerals seen" << mineralsSeen.size() << endl;
					for (std::set<Unidade*>::iterator i = mineralsSeen.begin(); i != mineralsSeen.end(); ++i){
						mineralsSet.insert((*i));
					}
					//mineralsSet.insert(mineralsSeen.begin(),mineralsSeen.end());
					
					cout << "minerals set + seen" << mineralsSet.size() << endl;
					//registra inimigos encontrados
					set<Unidade*> enemiesSeen = unidade->getEnemyUnits();
					for (std::set<Unidade*>::iterator i = enemiesSeen.begin(); i != enemiesSeen.end(); ++i){
						enemies.insert((*i));
					}
				}

				pegueiAcesso++;
				if (! ReleaseMutex(mineralsSetMutex)) { 
					// Handle error. Heh.
				}		
		}
	}
}

/*
void makeScout(Unidade* u){

	//pega posi��o, onde x e y correspondem ao indexes "i" e "j" do quadrant atualdo scout
	BWAPI::Position q = getQuadrant(u);
	int i = q.x();
	int j = q.y();

	//pega em quantos quadrantes o mapa esta divido
	int quadrant_lines = sizeof(quadrant);
	int quadrant_columns = sizeof(quadrant[0]);

	//ja visitou este quadrante inicial
	quadrant[i][j] = 1;

	int visited = 1;

	//global/quadrant search
	bool exploring = true;

	//local search
	bool exploting = true;

	BWAPI::Position p;
	//enquanto n�o visitou todos os quadrantes e est� explorando (pode parar antes de todos, caso haja algum inating�vel)
	while(visited < quadrant_lines*quadrant_lines && exploring){

		//inicia uma busca local no quadrante atual
		int localPoints = 4;
		while(exploting){

			BWAPI::Position next;
			do{
				//aleatoriamente - ESQ e DIR
				int coef_x = (rand() % 2);
				if(coef_x == 0) coef_x = -1;
				else coef_x = 1;

				//aleatoriamente - CIMA E BAIXO
				int coef_y = (rand() % 2);
				if(coef_y == 0) coef_y = -1;
				else coef_y = 1;

				//DEFINE QUANTO VAI ANDAR
				int walk_x = (rand() % 4 +1)*tileSize*coef_x;//sorteio numero de 1 a 3 tiles
				int walk_y = (rand() % 4 +1)*tileSize*coef_y;//sorteio numero de 1 a 3 tiles

				//TENTA ANDAR LOCALMENTE com os valores calculados
				next = BWAPI::Position(u->getPosition().x()+walk_x,u->getPosition().y()+walk_y);
				printf("LOCAL: Scout decided go to: %d px %d px\n", next.x(),next.y());
				localPoints--;
			}while(!u->hasPath(next));

			u->rightClick(next);
			//enquanto nao chega perto do ponto de destino atualiza lista de inimigos e minerais
			while(u->getDistance(next)> 10){
				updateEnviromentInfo(u);
			}

			//se ja visitou o total de pontos localmente, sair do laco
			if(localPoints == 0){
				exploting = false;
			}
		}

		// A busca local pode tirar o scout do quadrante inicial, atualizar
		q = getQuadrant(u);
		int i = q.x();
		int j = q.y();

		//SE CHEGOU AQUI, SIGNIFICA Q PAROU A BUSCA LOCAL, e VAI PARA UM QUADRANTE VIZINHO NAO VISITADO
		do{

			//escolher prox quadrante a visitar
			if(j+1 < quadrant_columns && quadrant[i][j+1] == 0)
				//visitar quadrante a direita
				p = getQuadrantCentroid(i,++j);

			else if(i+1 < quadrant_lines && j+1 < quadrant_columns && quadrant[i+1][j+1] == 0){
				//visitar quadrante diag a direita inferior
				p = getQuadrantCentroid(++i,++j);

			}else if(i+1 < quadrant_lines && quadrant[i+1][j] == 0){
				//visitar quadrante exatamente inferior
				p = getQuadrantCentroid(++i,j);

			}else if(i+1 < quadrant_lines && j-1 >=0 && quadrant[i+1][j-1] == 0){
				//visitar quadrante diag a esquerda inferior
				p = getQuadrantCentroid(++i,--j);

			}else if(j-1 >=0 && quadrant[i][j-1] == 0){
				//visitar quadrante a esquerda
				p = getQuadrantCentroid(i,--j);

			}else if(i-1>= 0 && j-1 >=0 &&  quadrant[i-1][j-1] == 0){
				//visitar quadrante diag a esquerda sup
				p = getQuadrantCentroid(--i,--j);

			}else if(i-1>= 0 &&  quadrant[i-1][j] == 0){
				//visitar quadrante exatemente sup
				p = getQuadrantCentroid(--i,j);
			}else{
				printf("Todos os quadrantes vizinhos JA FORAM EXPLORADOS\n");
				exploring = false;
			}
		}while(!u->hasPath(p));

		printf("GLOBAL: Scout decided go to: %d px %d px\n", p.x(),p.y());
		u->rightClick(p);
		exploring = true;

		while(exploring){

			updateEnviromentInfo(u);

			//se chegou perto do meio do quadrant, considerar visitado
			if(u->getDistance(p) < 50){
				exploring = false;
				quadrant[i][j] = 1;
				visited++;
			}
		}
		//atualiza quadrant atual
		q = getQuadrant(u);
		i = q.x();
		j = q.y();
	}
}
*/

int sumWithBounds(int quadAtual, int quadNumber){
        int result = quadAtual;
        if(quadAtual++ < quadNumber){
                result++;
        }else if(quadAtual-- < 0){
                result--;
        }
        return result;
}
int getCenterX (int quadrX, int quadrW){
        int centerX = quadrX*quadrW + (((quadrX+1)*quadrW)-(quadrX*quadrW))/2;
        return centerX--;
}
 
int getCenterY (int quadrY, int quadrH){
        int centerY = quadrY*quadrH + (((quadrY+1)*quadrH)-(quadrY*quadrH))/2;
        return centerY--;
}
 
/* void makeScout(Unidade* u){
        DWORD dwWaitResult, pegueiAcesso=0;
        dwWaitResult = WaitForSingleObject(
        scoutMutex,    // handle to mutex
        INFINITE);  // no time-out interval
       
        bool amIScout = true;
       	
		int quadNumberX = sizeof(quadrant);
		int quadNumberY = sizeof(quadrant[0]);

		
        //quadr � a altura/largura do quadrante
        int quadrH = mapH/quadNumberX;
        int quadrW = mapW/quadNumberY;

        int qnt = 0;

		BWAPI::TilePosition t;
		BWAPI::Position p;
        while(amIScout){
                t = u->getTilePosition();
                p = u->getPosition();
 
                int x = t.x(), y = t.y();
 
                //quadrAtual � o quadrante atual do scout (x,y)
                int quadrAtualX = x/quadrH;
                int quadrAtualY = y/quadrW;
 
                bool first = false;
                do{
 
                        int andarX = 0;
                        int andarY = 0;
                       /* if(quadrAtualY - 1 != 0 && quadrant[quadrAtualX][quadrAtualY] > quadrant[quadrAtualX][quadrAtualY-1]){
                                //andarX = getCenterX(quadrAtualX, quadrW);
                                //andarY = getCenterY(quadrAtualY-1, quadrH);
                                //quadrant[quadrAtualX][quadrAtualY-1]++;
								andarX = getCenterX(3, quadrW);
								andarY = getCenterY(3, quadrH);
 
                        }else if(quadrAtualX - 1 != 0 && quadrant[quadrAtualX][quadrAtualY] > quadrant[quadrAtualX-1][quadrAtualY]){
                                //andarX = getCenterX(quadrAtualX-1, quadrW);
								//andarY = getCenterY(quadrAtualY, quadrH);
                                //quadrant[quadrAtualX-1][quadrAtualY]++;
								andarX = getCenterX(3, quadrW);
								andarY = getCenterY(4, quadrH);
 
                        }else if(quadrAtualY + 1 < quadNumberY && quadrant[quadrAtualX][quadrAtualY] > quadrant[quadrAtualX][quadrAtualY+1]){
                                //andarX = getCenterX(quadrAtualX, quadrW);
                                //andarY = getCenterY(quadrAtualY+1, quadrH);
                                //quadrant[quadrAtualX][quadrAtualY+1]++;
								andarX = getCenterX(3, quadrW);
								andarY = getCenterY(3, quadrH);
                        }else if(quadrAtualX + 1 < quadNumberX  && quadrant[quadrAtualX][quadrAtualY] > quadrant[quadrAtualX+1][quadrAtualY]){
                                //andarX = getCenterX(quadrAtualX+1, quadrW);
                                //andarY = getCenterY(quadrAtualY, quadrH);
                                //quadrant[quadrAtualX+1][quadrAtualY]++;
								andarX = getCenterX(3, quadrW);
								andarY = getCenterY(3, quadrH);
                        }else{
							if(first){//mantem a mesma estrategia para andar
									qnt ++; //se ele entrou aqui, eh pq ja deu uma volta nesse loop, entao a estrategia para andar eh ruim, use a proxima
							}else{
									first = true;
							}
	                       
							if(qnt%4 == 0){
									andarX = (x+250)/32;
									andarY = (y+250)/32;
							}else if(qnt%4 == 1){
									andarX = (x-250)/32;
									andarY = y/32;
							}else if(qnt%4 == 2){
									andarX = (x+250)/32;
									andarY = (y-250)/32;
							}else if(qnt%4 == 3){
									andarX = x/32;
									andarY = (y+250)/32;
							} 
                        
                       
                        //int andarX = (sumWithBounds(quadrAtualX, quadNumberX))*quadrH + quadrH/2;
                        //int andarY = (sumWithBounds(quadrAtualY, quadNumberY))*quadrW + quadrW/2;
                       
 
                        p = BWAPI::Position(andarX*32,andarY*32);
                        cout << andarX << " " << andarY << endl;
                }while(!u->hasPath(p));
                        u->rightClick(p);

						while(u->isMoving()){
							updateEnviromentInfo(u);
						}

						    t = u->getTilePosition();
			 
							x = t.x();
							y = t.y();
			 
							//quadrAtual � o quadrante atual do scout (x,y)
							quadrAtualX = x/quadrH;
							quadrAtualY = y/quadrW;
			 
							//ja androu, atualiza o quadrante atual
							quadrant[quadrAtualX][quadrAtualY]++;
 
        }
        delete[] quadrant;
}
*/

void makeScout(Unidade* u){
	//pega em quantos quadrantes o mapa esta divido
	int quadrant_lines = sizeof(quadrant);
	int quadrant_columns = sizeof(quadrant[0]);

 	bool amIScout = true;
	while(amIScout){
		BWAPI::Position q = getQuadrant(u);
		int i = q.x();
		int j = q.y();
		
		//ja visitou este quadrante inicial
		quadrant[i][j] = 1;

		int visited = 1;

		BWAPI::Position p;

		do{

			//escolher prox quadrante a visitar
			if(j+1 < quadrant_columns && quadrant[i][j+1] == 0)
				//visitar quadrante a direita
				p = getQuadrantCentroid(i,++j);

			else if(i+1 < quadrant_lines && j+1 < quadrant_columns && quadrant[i+1][j+1] == 0){
				//visitar quadrante diag a direita inferior
				p = getQuadrantCentroid(++i,++j);

			}else if(i+1 < quadrant_lines && quadrant[i+1][j] == 0){
				//visitar quadrante exatamente inferior
				p = getQuadrantCentroid(++i,j);

			}else if(i+1 < quadrant_lines && j-1 >=0 && quadrant[i+1][j-1] == 0){
				//visitar quadrante diag a esquerda inferior
				p = getQuadrantCentroid(++i,--j);

			}else if(j-1 >=0 && quadrant[i][j-1] == 0){
				//visitar quadrante a esquerda
				p = getQuadrantCentroid(i,--j);

			}else if(i-1>= 0 && j-1 >=0 &&  quadrant[i-1][j-1] == 0){
				//visitar quadrante diag a esquerda sup
				p = getQuadrantCentroid(--i,--j);

			}else if(i-1>= 0 &&  quadrant[i-1][j] == 0){
				//visitar quadrante exatemente sup
				p = getQuadrantCentroid(--i,j);
			}else{
				int p = (rand() % quadrant_lines);
				int q = (rand() % quadrant_columns);
				p = getQuadrantCentroid(p,q);
			}
		}while(!u->hasPath(p));
		u->rightClick(p);

		while(u->isMoving()){
			updateEnviromentInfo(u);
		}
	}
}



//op 1 - reserve (soma)
//op 2 - release (subtrai)
bool reserveMinerals(int totalMinerals, int mineralCost, int op){
	DWORD dwWaitResult, pegueiAcesso=0; 
	dwWaitResult = WaitForSingleObject( 
		reserveMineralsMutex,   // handle to mutex
		INFINITE);				// no time-out interval

	while( pegueiAcesso < 1 ){ 
		switch (dwWaitResult) {
			// The thread got ownership of the mutex
			case WAIT_OBJECT_0: 		
				if(totalMinerals - mineralCost > 0){
					if(op == 1){
						mineralsReserved += mineralCost;
					}else if(op == 2){
						mineralsReserved -= mineralCost;					
					}

					if (! ReleaseMutex(reserveMineralsMutex)) { 
						// Handle error. Heh.
					}				
					return true;
				}else{
					if (! ReleaseMutex(reserveMineralsMutex)) { 
						// Handle error. Heh.
					}
					return false;
				}
				//NAO VAI NEM CHEGAR AQUI!
		}
	}
	return false;
}

bool reserveGas(int totalGas, int gasCost){
	if(totalGas - gasCost > 0){
		gasReserved += gasCost;
		return true;
	}
	return false;
}

void construir(BWAPI::UnitType type, Unidade* u,int price){
	int adi = 6;
	int limite = 0;
	//Construir algo em algum lugar
	while(true){
		BWAPI::TilePosition tp = u->getTilePosition();

		if((u)->build(tp,type)){
			//mineralsReserved -= mineralsPylonPrice;
			if(type == BWAPI::UnitTypes::Protoss_Gateway){
				newGatewayRequest--;
			}else if(type == BWAPI::UnitTypes::Protoss_Pylon){
				newPylonRequest--;
			}
			//Desalocando recurso
			reserveMinerals(u->minerals(), price, 2);
			break;
		}

		int randFactor = rand() % 100;
		if(randFactor < 50)
			tp = BWAPI::TilePosition(tp.x(), tp.y()+adi);
		else
			tp = BWAPI::TilePosition(tp.x()+adi, tp.y());
		tp.makeValid();
		limite++;
		if(limite > 50) break;
		adi = -adi + (adi > 0 ? -2 : +2);
	}

	amigoDaVez = NULL;//Bug aqui: amigoDaVez vai ser escolhido de novo antes mesmo do predio terminar...
}

void AIConstrutora (Unidade* u){

	//por conveniencia alguns metodos de Player estao sendo providos diretamente para as unidades. Vide inicio de Unidade.h
	BWAPI::TilePosition tp = u->getTilePosition();

	//O QUE CONSTRUIR?

	BWAPI::UnitType type;

	// adicionar todas as constru��es que vai precisar como elses if

	if(u->supplyTotal() - u->supplyUsed() < 5){
		int price = BWAPI::UnitTypes::Protoss_Pylon.mineralPrice();
		if(reserveMinerals(u->minerals(),price,1)){
			type = BWAPI::UnitTypes::Protoss_Pylon;
			//printf("Iniciar constru��o de pylon\n");
			construir(type,u,price);
		}else{
			//printf("Nao teve recursos para pylon\n");
		}
	}else if(newGatewayRequest > 0){
		//u->minerals() - reserveMinerals >= 150  &&
		int price = BWAPI::UnitTypes::Protoss_Gateway.mineralPrice();
		if(reserveMinerals(u->minerals(),price,1)){
			//printf("Iniciar constru��o de gateway\n");
			type = BWAPI::UnitTypes::Protoss_Gateway;
			construir(type,u,price);
		}else{
			//printf("Nao teve recursos para gateway\n");
		}
	}else{
		//printf("N�o construir nada\n");
	}
}

void AICentroComando (Unidade* u){

	BWAPI::UnitType type = u->getType();

	if(type == BWAPI::UnitTypes::Protoss_Nexus && newProbeRequest > 0 && u->minerals() >= BWAPI::UnitTypes::Protoss_Probe.mineralPrice()){
		if(u->train(BWAPI::UnitTypes::Protoss_Probe)) newProbeRequest--;
	}else if(type == BWAPI::UnitTypes::Protoss_Gateway){
		if(u->minerals()- 100 && u->train(BWAPI::UnitTypes::Protoss_Zealot)){
			newZealotRequest--;
		}
	}

	//checkForDeadProbesToReplce
	for(int i=0; i < probes.size();i++){
		//se est� morto, tira da lista e pede um novo no papel dele.
		if(!probes[i].unidade->exists()){

			//QUANDO CRIAR O PROBE FAZER
			/*Actor a;
			a.unidade = probes[i].unidade;
			a.role = probes[i].role;*/

			probes.erase(probes.begin()+i);
			newProbeRequest++;
		}
	}

	if (scout!= NULL && !scout->exists()) scout = NULL;

	//u->train(BWAPI::UnitTypes::Protoss_Zealot);//se for um gateway
	if((u->supplyTotal() - u->supplyUsed() < 5 || u->minerals() > BWAPI::UnitTypes::Protoss_Pylon.mineralPrice()) && amigoDaVez == NULL && scout == NULL){
		newPylonRequest++;
		//botar no "blackboard" para alguem construir predios de supply
		std::set<Unidade*> amigos = u->getAllyUnits();

		for(std::set<Unidade*>::iterator it = amigos.begin(); it != amigos.end(); it++){
			if((*it)->getType().isWorker()){
				printf("amigoDaVez set\n");
				amigoDaVez = *it;
				break;
			}
		}
	}//Lembrar que ha varias threads rodando em paralelo. O erro presente neste metodo (qual?) nao resulta em crash do jogo, mas outros poderiam.
}

void AIGuerreiro (Unidade* u){
	//Adailson aqui-> REGISTRAR INIMIGOS EM VARIAVEL GLOBAL NO BLACKBOARD, ATUALIZAR LISTA TIRANDO OS MORTOS E COLOCANDO OS NOVOS
	if(enemies.size()>0){

		set<Unidade*>::iterator it;
		for (it = enemies.begin(); it != enemies.end(); ++it)
		{
			BWAPI::UnitType tipo = (*it)->getType();
			if(tipo == BWAPI::UnitTypes::Protoss_Probe)
				u->attack(*it);
			//Nao desperdicar threads com predios que nao fazem nada
		}

	} //ataca uma unidade inimiga aleatoria. Assume que existe uma.
	//cuidado com bugs como este. O codigo acima daria crash de NULL pointer no exato momento que o time inimigo
	//nao possuisse mais unidades, antes da partida de fato acabar.
}

DWORD WINAPI threadAgente(LPVOID param){

	Unidade *u = (Unidade*) param;

	//exemplos de metodos uteis para construir predios
	/*bool x = u->hasPower(3,4,50,60);
	u->isBuildable(50,50);
	u->isBuildable(BWAPI::TilePosition(3,5));*/

	//A classe Agente Principal ainda tem o metodo AgentePrincipal bool isWalkable(int x, int y).

	while(true){
		//Se houve algum problema (ex: o jogo foi fechado) ou a unidade estah morta, finalizar a thread
		if(GameOver || u == NULL || !u->exists()) return 0;

		//Enquanto a unidade ainda nao terminou de ser construida ou o seu comando ainda nao foi
		//processado (ou seja, continua no mesmo turno), aguardar e poupar processamento
		if(!u->isCompleted()){
			Sleep(500);
			continue;
		}
		if(!u->checkNovoTurno()){
			Sleep(10);
			continue;
		}
		//Inserir o codigo de voces a partir daqui//

		if(u->isIdle()){ //nao ta fazendo nada, fazer algo util
			if(u == amigoDaVez){
				printf("Constutor\n");
				AIConstrutora(u);
			}else if(u == scout){
				printf("Scout\n");
				makeScout(u);
			}else if(u->getType().isWorker()){
				printf("Trabalhador\n");
				AITrabalhador(u);
			}else if(u->getType().canProduce()){
				//printf("Comando\n");
				AICentroComando(u);
			}else{
				printf("Guerreiro\n");
				AIGuerreiro(u);
			}
		}
		else if(u->getType().isWorker() && u != scout) AIConstrutora(u); //construir msm q estivesse fazendo algo
		Sleep(10);//Sempre dormir pelo menos 10ms no final do loop, pois uma iteracao da thread � muito mais r�pida do que um turno do bwapi.
	}
}

void MeuAgentePrincipal::InicioDePartida(){
	//Inicializar estruturas de dados necessarias, ou outras rotinas de inicio do seu programa. Cuidado com concorrencia, 
	//em alguns casos pode ser recomendavel que seja feito antes do while na criacao de uma nova thread.
	scoutMutex = CreateMutex( 
		NULL,              // default security attributes
		FALSE,             // initially not owned
		NULL);				// unnamed mutex

	reserveMineralsMutex = CreateMutex( 
		NULL,              // default security attributes
		FALSE,             // initially not owned
		NULL);				// unnamed mutex

	mineralsSetMutex = CreateMutex( 
		NULL,              // default security attributes
		FALSE,             // initially not owned
		NULL);				// unnamed mutex

	GameOver = false;
	tileSize = 32;
	mapH = AgentePrincipal::mapHeight();
	mapW = AgentePrincipal::mapWidth();

	//divide o mapa em quadrantes
	if(mapH == mapW){//e mapa for quadrado
		if(mapH > 100){
			int amount = 8;
			quadrant = new int*[amount];
			for (int i = 0; i < amount; ++i) {
				quadrant[i] = new int[amount];
				memset(quadrant[i], 0, amount*sizeof(int));
			}
		}else if(mapH > 50){
			int amount = 4;
			quadrant = new int*[amount];
			for (int i = 0; i < amount; ++i) {
				quadrant[i] = new int[amount];
				memset(quadrant[i], 0, amount*sizeof(int));
			}
		}else{
			int amount = 2; 
			quadrant = new int*[amount];
			for (int i = 0; i < amount; ++i) {
				quadrant[i] = new int[amount];
				memset(quadrant[i], 0, amount*sizeof(int));
			}
		}		
	}else{
		if(mapH > mapW){
			int amount = 8;
			quadrant = new int*[amount];
			for (int i = 0; i < amount; ++i) {
				quadrant[i] = new int[amount*2];
				memset(quadrant[i], 0, amount*sizeof(int));
			}
		}else{
			int amount = 4;
			quadrant = new int*[amount];
			for (int i = 0; i < amount; ++i) {
				quadrant[i] = new int[amount*2];
				memset(quadrant[i], 0, amount*sizeof(int));
			}
		}
	}

	printf("EEENOOXXX  VOOOOAAA, BRUXAOOO!!\n");
	printf("QUERIA-STAR-CRAFT!!\n");
	printf("MAP CONFIGURATION:\n");
	printf("%d hight\n",mapH);
	printf("%d width\n",mapW);

}

void MeuAgentePrincipal::onEnd(bool isWinner){  
	//sinalizar e aguardar o tempo necessario para todas as threads se terminarem antes do jogo sair, evitando erros.
	GameOver = true;
	Sleep(550);
}

void MeuAgentePrincipal::UnidadeCriada(Unidade* unidade){
	//Uma nova unidade sua foi criada (isto inclui as do inicio da partida). Implemente aqui como quer tratar ela.
	BWAPI::UnitType tipo = unidade->getType();

	//Nao desperdicar threads com predios que nao fazem nada
	if(tipo != BWAPI::UnitTypes::Protoss_Pylon && tipo != BWAPI::UnitTypes::Protoss_Assimilator){
		CreateThread(NULL,0,threadAgente,(void*)unidade,0,NULL);
		if(tipo == BWAPI::UnitTypes::Protoss_Nexus){
			if(nexus == NULL){

				DWORD dwWaitResult, pegueiAcesso=0; 
					dwWaitResult = WaitForSingleObject( 
					mineralsSetMutex,   // handle to mutex
					INFINITE);			// no time-out interval

				while( pegueiAcesso < 1 ){ 
					switch (dwWaitResult) {
						// The thread got ownership of the mutex
						case WAIT_OBJECT_0: 
							//Se este mineral acabou
							nexus = unidade;
							//Inserir o codigo de voces a partir daqui//
							set<Unidade*> mineralsSeen = nexus->getMinerals() ;
							printf("AAAAAAAAAAAAAAAAAAAA vou adicionar %d minerais visto pelo nexus \n", mineralsSeen.size());
							
							for (std::set<Unidade*>::iterator i = mineralsSeen.begin(); i != mineralsSeen.end(); ++i){
								cout<<((*i)->getPosition().x())<<" ";
								mineralsSet.insert((*i));
							}

							printf("AAAAAAAAAAAAAAAAAAAA o nexus adicionou e agora tem %d minerais na variavel \n", mineralsSet.size());
							pegueiAcesso++;
							if (! ReleaseMutex(mineralsSetMutex)) { 
									// Handle error. Heh.
							}
					}
				}
			}
		}else if(tipo == BWAPI::UnitTypes::Protoss_Probe){
			if(scout == NULL){
				Actor a;
				a.unidade = unidade;
				a.role = SCOUT;
				probes.push_back(a);
				scout = unidade;
			}else{

			}
			Actor a;
			a.unidade = unidade;
			a.role = COLLECTOR;
			probes.push_back(a);
		}
	}
	else{
		//talvez voce queira manter a referencia a estes predios em algum lugar
	}
}

wstring s2ws(const std::string& s){
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

void debug (std::string s){
	wstring stemp = s2ws(s);
	OutputDebugString(stemp.c_str());
}

/*
Os outros eventos nao existem numa arquitetura orientada a Agentes Autonomos, pois eram relacionados ao Player do Broodwar
de maneira generica, nao sendo especificamente ligados a alguma unidade do jogador. Se desejado, seus comportamentos podem
ser simulados atrav�s de t�cnicas ou estruturas de comunica��o dos agentes, como por exemplo Blackboards.
*/
